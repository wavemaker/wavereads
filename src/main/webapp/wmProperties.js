var _WM_APP_PROPERTIES = {
  "activeTheme" : "bricks",
  "defaultLanguage" : "en",
  "displayName" : "WaveReads",
  "homePage" : "Main",
  "name" : "wavereads",
  "platformType" : "MOBILE",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};