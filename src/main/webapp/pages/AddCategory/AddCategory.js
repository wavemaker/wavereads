Application.$controller("AddCategoryPageController", ["$scope", "$timeout", "wmToaster", function($scope, $timeout, wmToaster) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };





    function addCategory() {
        $scope.Variables.addCategory.insertRecord();
        $scope.Variables.LIBRARYCategoryData.update();
        $scope.Variables.CATEGORY_LIST.update();
        $scope.Variables.TotalCategories.update();

    }


    $scope.categoryNameKeydown = function($event, $isolateScope) {
        if ($event.keyCode === 13 && $event.currentTarget.value.length > 0) {
            $isolateScope.datavalue = $event.currentTarget.value;
            $timeout(addCategory, 100);
        }
    };


    $scope.anchor1Click = function($event, $isolateScope) {
        addCategory();
    };


    $scope.addCategoryonSuccess = function(variable, data) {
        wmToaster.show('success', 'SUCCESS', 'Category is added successfully.');
    };

}]);


Application.$controller("addCategoryFormController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);