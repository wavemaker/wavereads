Application.$controller("MainPageController", ["$scope", "$timeout", "$rootScope", function($scope, $timeout, $rootScope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };


    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */

    };


    $scope.bookPicClick = function($event, $isolateScope) {
        $scope.Variables.bookDetailsPage.dataSet.book = $isolateScope.$parent.item;
        $timeout(function() {
            $scope.Variables['goToPage-Details'].navigate();
        }, 100);
    };


    $scope.mobile_navbar1Search = function($event, $isolateScope) {
        $timeout(function() {
            $scope.Variables['goToPage_SearchByAPI'].navigate();
        }, 1000);
    };

}]);