Application.$controller("CommonPageController", ['$scope', '$rootScope', '$cordovaNetwork', "CONSTANTS",
    function($scope, $rootScope, $cordovaNetwork, CONSTANTS) {
        "use strict";

        // function checkOnlineStatus() {
        //     if (CONSTANTS.hasCordova) {
        //         function checkOnlineStatus() {
        //             $scope.$root.Variables.isAppOnline.dataSet.dataValue = $cordovaNetwork.isOnline();
        //         }
        //     }
        // }
        // $scope.$root.onAppVariablesReady = function() {
        //     checkOnlineStatus();
        // }


        /* perform any action on widgets within this block */
        $scope.onPageReady = function() {

            /*
             * widgets can be accessed through '$scope.Widgets' property here
             * e.g. to get value of text widget named 'username' use following script
             * '$scope.Widgets.username.datavalue'
             */
        };

        //     $scope.retryLinkClick = function($event, $isolateScope) {
        //         $scope.$root.Variables.showOfflineMessage.dataSet.dataValue = false;
        //         checkOnlineStatus();
        //     };
        //     /*** TODO in Wavemaker : Provide a variable that tells whether the app is online or offline.. **/
        //     $rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
        //         $scope.$root.Variables.isAppOnline.dataSet.dataValue = false;
        //         WM.element('body').addClass('wm-app-offline');
        //     });

        //     $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
        //         WM.element('body').removeClass('wm-app-offline');
        //         $scope.$root.Variables.isAppOnline.dataSet.dataValue = true;
        //         $scope.$root.Variables.showOfflineMessage.dataSet.dataValue = false;
        //         $rootScope.$root.Variables.tenantInfo.update();
        //     });

    }
]);

Application.$controller("CommonLoginDialogController", ["$scope", "DialogService", "$window",
    function($scope, DialogService, $window) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.CommonLoginDialogError = function($event, $isolateScope) {
            /*
             * Error message can be accessed from the property $isolateScope.loginMessage.caption
             */
        };

        $scope.CommonLoginDialogSuccess = function($event, $isolateScope) {
            /*
             * This success handler provides a redirectUrl which is the role based url set while configuring Security service for the project.
             * The redirectUrl can be accessed as $isolateScope.redirectUrl
             * To navigate to the url use '$window' service as:
             * $window.location = $isolateScope.redirectUrl
             */
            DialogService.hideDialog("CommonLoginDialog");
        };
    }
]);