Application.$controller("CategoryItemsPageController", ["$scope", "$timeout", function($scope, $timeout) {
    "use strict";

    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
        $scope.Variables.LIBRARYExecuteGetCategoryList.update();
    };



    $scope.categoryCoverClick = function($event, $isolateScope) {
        $timeout(function() {
            $scope.Variables.goToPage_CategoryBook.navigate();
        }, 100);
    };

}]);