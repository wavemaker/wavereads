Application.$controller("DetailsPageController", ["$scope", 'wmToaster', '$timeout', '$injector', 'CONSTANTS', '$cordovaDialogs', 'Utils', '$rootScope', function($scope, wmToaster, $timeout, $injector, CONSTANTS, $cordovaDialogs, Utils, $rootScope) {
    "use strict";
    var parentView;
    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */

    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        var detailsPage = WM.element('.book-detailspage'),
            detailsContent = detailsPage.find('[name="BookHeader23"]:first');

        parentView = detailsPage.closest('.app-view').isolateScope();
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */

        if (parentView) {
            parentView.$watch('show', function(newValue) {
                if (newValue) {
                    document.addEventListener("backbutton", $scope.goToPrevious, false);
                    $timeout(function() {
                        detailsContent.show();
                    }, 100);
                } else {
                    document.removeEventListener("backbutton", $scope.goToPrevious, false);
                    $timeout(function() {
                        $scope.Widgets.bookPicture.imagesource = 'resources/images/imagelists/Icon_default%20book3x.png';
                        $scope.Variables.bookDetailsPage.dataSet.book = {};
                        detailsContent.hide();
                    }, 200);
                }
            });
        }
    };
    $scope.goToPrevious = function(event) {
        if (parentView && parentView.show) {
            parentView.show = false;
            if (event) {
                event.stopPropagation();
                $scope.$apply();
            }
        } else if (!event) {
            window.history.go(-1);
        }
    };

    //handle mobile navbar back button click
    $scope.mobile_navbar1Backbtnclick = function($event, $isolateScope) {
        $scope.goToPrevious();
    };

    //handle delete button
    $scope.deleteBookonSuccess = function(variable, data) {
        $scope.goToPrevious();
        $scope.$emit('deleteBookSuccess');
        wmToaster.show('success', 'SUCCESS', 'Book is deleted successfully.');
    };

    //handle back button
    document.addEventListener("backbutton", $scope.goToPrevious, false);

    $scope.$on('$destroy', function() {
        document.removeEventListener("backbutton", $scope.goToPrevious, false);
    });


    $scope.deleteBtnClick = function($event, $isolateScope) {
        if (CONSTANTS.hasCordova && Utils.isIphone()) {
            $cordovaDialogs.confirm('Would like to delete the book?')
                .then(function(buttonIndex) {
                    //OK
                    if (buttonIndex === 1) {
                        $scope.Variables.deleteBook.deleteRecord();
                    }
                });
        } else if (confirm('Would like to delete the book?')) {
            $scope.Variables.deleteBook.deleteRecord();
        }
    };


    $scope.editBtnClick = function($event, $isolateScope) {
        $scope.Variables.addBookPage.bookData = $scope.Variables.bookDetailsPage.dataSet.book;
    };



    $scope.LIBRARYUpdateCategoryDetailsonSuccess = function(variable, data) {
        $scope.$emit('bookCategoryChangeSuccess');
        wmToaster.show('success', 'SUCCESS', 'Category is updated successfully.');
        $timeout(function() {
            $injector.get('wmSpinner').hide();
        }, 300);
    };


    $scope.updateBookStatusonSuccess = function(variable, data) {
        wmToaster.show('success', 'SUCCESS', 'Book status is updated successfully.');
        $timeout(function() {
            $injector.get('wmSpinner').hide();
        }, 300);
    };


    $scope.readStatusChange = function($event, $isolateScope) {
        $injector.get('wmSpinner').show();
    };


    $scope.updateBookRatingonSuccess = function(variable, data) {
        wmToaster.show('success', 'SUCCESS', 'Book rating is updated successfully.');
        $timeout(function() {
            $injector.get('wmSpinner').hide();
        }, 300);
    };


    $scope.select5Change = function($event, $isolateScope) {
        $injector.get('wmSpinner').show();
    };


    $scope.rating1Change = function($event, $isolateScope) {
        $injector.get('wmSpinner').show();
    };
    $scope.addBtnClick = function($event, $isolateScope) {
        $scope.Variables.bookDetailsPage.dataSet.book.id = undefined;
        $scope.Variables.addBookPage.bookData = $scope.Variables.bookDetailsPage.dataSet.book;
    };

}]);